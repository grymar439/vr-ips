﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VRMap
{
    public Transform vrTarget;
    public Transform rigTarget;
    public Vector3 trackingPositionOffset;
    public Vector3 trackingRotationOffset;

    public void Map()
    {
        rigTarget.position = vrTarget.TransformPoint(trackingPositionOffset);
        // if (rigTarget.position.x > 0)
        // {
        //     rigTarget.position.x = 0;
        // }
        // if (rigTarget.position.y > 90)
        // {
        //     rigTarget.position.y = 90;
        // }
        rigTarget.rotation = vrTarget.rotation * Quaternion.Euler(trackingRotationOffset);
        Debug.Log("Rot"+ rigTarget.rotation);
    }
}

public class VRRig : MonoBehaviour
{

    public VRMap head;
    public VRMap leftHand;
    public VRMap rightHand;

    public Transform headConstraint;
    public Vector3 headBodyOffset;

    // Start is called before the first frame update
    void Start()
    {
        headBodyOffset = transform.position - headConstraint.position;
        if (headBodyOffset.y < 0.0f)
        {
            headBodyOffset.y = 0.0f;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        head.Map();
        leftHand.Map();
        rightHand.Map();
        Debug.Log("HeadBodyOffset"+ headBodyOffset);
    }
}
